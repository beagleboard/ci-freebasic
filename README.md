# ci-freebasic


# Add repo to your apt sources.list

```
deb [trusted=yes] https://beagleboard.beagleboard.io/ci-freebasic stable main
```

# Quick One line:

```
sudo sh -c "echo 'deb [trusted=yes] https://beagleboard.beagleboard.io/ci-freebasic stable main' > /etc/apt/sources.list.d/ci-freebasic.list"
```

#

